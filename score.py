import os
import torch as th
import transformers
import pytorch_lightning as pl
import json


from azureml.core.model import Model

# MODEL_NAME
# MAX_LENGTH
# NUM_LABELS
# CLASSES mit ARGPARS

class HandtmannNLPClassifier(pl.LightningModule):

      def __init__(self):
        super().__init__()

        #changing the configuration to X lables instead of 2
        self.bert = transformers.BertModel.from_pretrained("bert-base-german-cased")
        self.drop = th.nn.Dropout(p=0.1)
        self.out = th.nn.Linear(self.bert.config.hidden_size, 10)
        self.softmax = th.nn.Softmax(dim=1)
        self.loss = th.nn.CrossEntropyLoss(reduction="none")
      
      def forward(self, input_ids, mask):
        print("input: ", input_ids)
        _, pooled_output = self.bert(
                                    input_ids=input_ids,
                                    attention_mask=mask
                            )
        output= self.drop(pooled_output)
        output = self.out(output)
        return self.softmax(output)


def init():
    global model
    # AZUREML_MODEL_DIR is an environment variable created during deployment.
    # It is the path to the model folder (./azureml-models/$MODEL_NAME/$VERSION)
    # For multiple models, it points to the folder containing all deployed models (./azureml-models)
    model_path = os.path.join(
        os.getenv('AZUREML_MODEL_DIR'), 'ticket-class-bert.pt')
    model = HandtmannNLPClassifier()
    model.load_state_dict(th.load(model_path, map_location=lambda storage, loc: storage))
    model.eval()


def run(input):
    tokenizer = transformers.AutoTokenizer.from_pretrained(
        "bert-base-german-cased")


    data = json.loads(input)['data']
    text = str(data)
    text = " ".join(text.split())

    input_data = tokenizer(
        text,
        None,
        add_special_tokens=True,
        max_length=200,
        pad_to_max_length=True,
        return_token_type_ids=True,
        truncation=True,
        padding='max_length',
        return_tensors="pt"
    )

    input_ids = input_data['input_ids']
    mask = input_data['attention_mask']
    
    # get prediction
    output = model(input_ids, mask)
    
    classes = ['Baan', 'Babtec', 'CAD / PLM', 'E-Mail', 'Intranet / Sharepoint',
               'Office', 'SAP', 'Hardware', 'Telefonie', 'Zugriffe / Berechtigungen']

    # Convert tensor to integer
    index = th.argmax(output, -1).tolist()[0]

    result = {"label": classes[index], "data": text, "index": index}
    return result
